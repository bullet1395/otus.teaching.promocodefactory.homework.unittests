﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_PatrnerIsNotFound_ReturnNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            Partner partner = null;
            var request = new SetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_PatrnerIsNotActive_ReturnBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            var partner = PartnerBuilder.CreateNotActivePartner();
            var request = new SetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_PatrnerIsSetNewLimit_ShouldNumberIssuedPromoCodesSetZero()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            var partner = PartnerBuilder.CreateBasePartner();
            var request = new SetPartnerPromoCodeLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_PatrnerIsSetNewLimit_ShouldOffPreviosLimit()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            var partner = PartnerBuilder.CreateBasePartner().WithOnlyOneActiveLimit();
            var activeLimit = partner.PartnerLimits.First();

            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now.AddDays(7),
                Limit = 10
            };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            activeLimit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_LimitShouldBeNotZero_ReturnBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            var partner = PartnerBuilder.CreateBasePartner();

            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now.AddDays(7),
                Limit = 0
            };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimit_LimitIsSaveDb_ReturnCreated()
        {
            // Arrange
            var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");
            var partner = PartnerBuilder.CreateBasePartner();

            var request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now.AddDays(7),
                Limit = 18
            };

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            _partnersRepositoryMock.Setup(repo => repo.UpdateAsync(partner));

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }
    }
}